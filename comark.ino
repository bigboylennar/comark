#include <MFRC522.h>


#include <EmonLib.h>
#include <LiquidCrystal_I2C.h>
#include<SPI.h>

#include <Wire.h> // For I2C
LiquidCrystal_I2C lcd(0x27,20,4);
EnergyMonitor emon;

//creating mfrc522 instance
#define RSTPIN 9
#define SSPIN 10
MFRC522 rc(SSPIN, RSTPIN);
int energy = 0; 

String inData = "";
String result = "";
byte readcard[4];
int pinOut = 7;

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
  lcd.setCursor(4,0);
  lcd.print("Room 401");
  lcd.setCursor(2,1);
  lcd.print("Tap your ID!");
  SPI.begin();
  rc.PCD_Init();
  emon.current(1, 19);
  pinMode(pinOut, OUTPUT);
}


void loop() {
  getid();
  while(Serial.available() > 0) {
    char received = Serial.read();
    if(received == '|') {
      lcd.clear();  
      lcd.print(inData);     
      inData = "";
    } else if(received == '(') {
      lcd.setCursor(0, 1);
      lcd.print(inData);
      inData = "";
    } else if(received == ')') {
      lcd.setCursor(8, 1);
      lcd.print(inData);
      if (inData == "off") {
        lcd.clear();
//        result = "-" + energy;
//        Serial.println(resu`lt);
//        energy = 0;
        digitalWrite(pinOut, LOW);
      } else if (inData == "on") {
//        int Irms = emon.calcIrms(1000);
//        int power = (Irms * (230/1000));
//        energy = ((power + energy) / 3600);
        digitalWrite(pinOut, HIGH);
      }
      inData = "";
    } else if (received == '*') {
      lcd.print("Time Consumed:");
      lcd.setCursor(0,1);
      lcd.print(inData);
      delay(1000);
      lcd.clear();
      lcd.setCursor(4,0);
      lcd.print("Room 401");
      lcd.setCursor(2,1);
      lcd.print("Tap your ID!");
      inData = "";
    }
      else {
      inData.concat(received);
    }
  }
}

int getid(){  
  if(!rc.PICC_IsNewCardPresent()){
    return 0;
  }
  if(!rc.PICC_ReadCardSerial()){
    return 0;
  }
  
  for(int i=0;i<4;i++){
    readcard[i]=rc.uid.uidByte[i];
    Serial.print(readcard[i],HEX); 
  }
  Serial.println("");
  rc.PICC_HaltA();
  return 0;
}
