import React from 'react'
import ReactDOM from 'react-dom'
import store from './redux/store'
import { Provider } from 'react-redux'
import Component from './redux/containers/Main'
import 'izitoast/dist/css/iziToast.min.css'
import 'izitoast/dist/js/iziToast.min.js'

function renderComponent(Component) {
	ReactDOM.render(
		<Provider store={store}>
				<Component />
		</Provider>,
		document.getElementById('root')
	)
}

renderComponent(Component)
