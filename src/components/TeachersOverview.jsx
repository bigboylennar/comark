import React, { Component } from 'react'
import { Table, TableCell, TableRow, TableBody, Button } from 'semantic-ui-react'

class Overview extends Component {
  componentDidMount() {
    this.props.getTeachers()
  }

  render() {
    const rows = this.props.teachers.map((teacher, index) => {
      return (
        <TableRow key={index}>
          <TableCell><b>{teacher.lastName}, {teacher.firstName}</b></TableCell>
          <TableCell>{teacher.employmentType}</TableCell>
          <TableCell>{teacher.idNumber}</TableCell>
          <TableCell>{teacher.department}</TableCell>
          <TableCell>{teacher.rfidCode}</TableCell>
          <TableCell>
            <Button 
              icon='edit'
              onClick={() => this.props.toggleEditTeacherModal(teacher._id)}
              circular
              // positive
            />
            <Button 
              icon='delete'
              circular
              onClick={() => this.props.deleteTeacher(teacher)}
              // negative
            />
          </TableCell>
        </TableRow >
      )
    })

    return (
      <Table celled fixed textAlign={'center'} selectable basic={'very'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Full Name</Table.HeaderCell>
            <Table.HeaderCell>Employment Type</Table.HeaderCell>
            <Table.HeaderCell>ID Number</Table.HeaderCell>
            <Table.HeaderCell>Department</Table.HeaderCell>
            <Table.HeaderCell>RFID Code</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <TableBody>
          {rows}
        </TableBody >
      </Table >
    )
  }
}

export default Overview