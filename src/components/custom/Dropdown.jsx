import React from 'react'
import { Form } from 'semantic-ui-react'

export const DropdownField = props => (
	<Form.Field>
		<Form.Dropdown
			selection
			{...props.input}
			options={props.options}
			value={props.input.value}
			onChange={(e, data) => props.input.onChange(data.value)}
			placeholder={props.label}
			required
			clearable
		/>
	</Form.Field>
)
