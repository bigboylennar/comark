export const departments = [
	{
		key: 'Mechanical Engineering',
		text: 'Mechanical Engineering',
		value: 'Mechanical Engineering'
	},
	{
		key: 'Electrical Engineering',
		text: 'Electrical Engineering',
		value: 'Electrical Engineering'
	},
	{
		key: 'Chemical Engineering',
		text: 'Chemical Engineering',
		value: 'Chemical Engineering'
	},
	{
		key: 'Electronics Engineering',
		text: 'Electronics Engineering',
		value: 'Electronics Engineering'
	},
	{
		key: 'Civil Engineering',
		text: 'Civil Engineering',
		value: 'Civil Engineering'
	},
	{
		key: 'Packaging Engineering',
		text: 'Packaging Engineering',
		value: 'Packaging Engineering'
	},
	{
		key: 'Software Engineering',
		text: 'Software Engineering',
		value: 'Software Engineering'
	}
]

export const employmentTypes = [
	{
		key: 'Part Time',
		value: 'Part Time',
		text: 'Part Time',
	},
	{
		key: 'Full Time',
		value: 'Full Time',
		text: 'Full Time'
	}
]