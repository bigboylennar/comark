import { connect } from 'react-redux'
import EditTeacherModal from '../../components/EditTeacherModal'
import { toggleEditTeacherModal, updateTeacherData } from '../actions/index'

function mapStateToProps(state) {
  return {
    isOpen: state.editModal.isOpen,
    initialValues: state.editModal.initialValues
  }
}

function mapDispatchToProps(dispatch) {
  return {
    toggleEditTeacherModal(id) {
      dispatch(toggleEditTeacherModal(id))
    },
    updateTeacherData(id, values) {
      dispatch(updateTeacherData(id, values))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTeacherModal)