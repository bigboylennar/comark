import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import teacherRegistrationReducer from './teacherRegistration'
import menuReducer from './menu'
import teachersReducer from './teachers'
import editTeacherModalReducer from './editModal'
import logsReducer from './logs'

export default combineReducers({
	form: formReducer,
	teacherRegistration: teacherRegistrationReducer,
	menu: menuReducer,
	teachers: teachersReducer,
	editModal: editTeacherModalReducer,
	logs: logsReducer
})
