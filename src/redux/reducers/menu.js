const initialState = {
  activeItem: 'register'
}

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case 'SET_ACTIVE_ITEM':
      return {
        ...state,
        activeItem: action.payload
      }
    default: 
      return {
        ...state
      }
  }
}