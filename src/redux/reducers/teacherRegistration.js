const initialState = {
  rfidCode: null,
  isScanning: false
}

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case 'SCANNING_RFID':
      return {
        ...state,
        rfidCode: null,
        isScanning: true
      }
    case 'RFID_SCAN_SUCCESS': 
      return {
        ...state,
        rfidCode: action.payload,
        isScanning: false
      }
    case 'RFID_SCAN_FAILURE':
      alert(action.payload)
      return {
        ...state
      }
    case 'REGISTER_TEACHER_SUCCESS':
      console.log('called...')
      return {
        ...state,
        rfidCode: null
      }
    default: 
      return {
        ...state
      }
  }
}